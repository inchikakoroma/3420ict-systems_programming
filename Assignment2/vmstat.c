//  Created by Rene Hexel on 11/05/2015.
//  Copyright (c) 2015 Rene Hexel. All rights reserved.
//  Edited by Inchika Alan Koroma on 22 May 2015

#include "vmstat.h"

/// read a long value from the given file
///
static inline long fread_long(FILE *file)
{
    char buffer[BUFFER_SIZE];               // response buffer

    if (fgets(buffer, sizeof(buffer), file))// read one line into buffer
        return atol(buffer);                // convert line to long

    return -1;                              // error
}

///
/// set a predefined search path for executables
///
static inline void set_path(void)
{
    setenv("PATH", "/bin:/sbin:/usr/bin:/usr/sbin", 1);
}

///
/// return configuration information (Linux)
/// returns -1 if unknown
///
static inline long getconf(const char *variable)
{
    long rv = -1;                           // return value
    char buffer[BUFFER_SIZE];               // command & response buffer

    // create command to run, then execute in pipe
    snprintf(buffer, sizeof(buffer), "/usr/bin/getconf %s", variable);
    set_path();                             // set execution path
    FILE *pconf = popen(buffer, "r");       // run getconf to get config
    if (!pconf) return rv;                  // error? -> -1

    rv = fread_long(pconf);                 // parse output as long

    pclose(pconf);                          // clean up

    return rv;                              // return configuration data
}


///
/// parse the free memory information
/// from a given command (cmd)
///
static long parse_cmd(const char *cmd)
{
    long rv = -1;                           // return value
    set_path();                             // set execution path
    FILE *pvmstat = popen(cmd, "r");        // run command
    if (!pvmstat) return rv;                // error? -> -1

    rv = fread_long(pvmstat);               // parse output as long

    pclose(pvmstat);                        // clean up

    return rv;                              // return page size
}

///
/// return the memory currently available in the system
///
memstat available_memory(void)
{
    memstat info;                           // available memory information
    memset(&info, 0, sizeof(info));         // set everything to zero

    info.page_size = getconf("PAGESIZE");   // get the page size configuration
    if (info.page_size <= 0)
        info.page_size = parse_cmd(VM_PAGE);// get page size from vm_stat

    // set the default page size if we cannot get it from the system
    if (info.page_size <= 0 && info.available_pages)
        info.page_size = DEFAULT_PAGE_SIZE;

    long avail = parse_cmd(VMSTAT) * 1024;  // available memory using vmstat
    if (avail <= 0)                         // error?
        avail = parse_cmd(VM_STAT);         // try vm_stat instead
    else
        avail /= info.page_size;            // convert to pages

    info.available_pages = avail;           // store in info

    return info;
}

void read_mem(memstat *locks, int id) {
    int i;
    for (i = 0; i < 10; i++) {
        sem_wait(locks->reader);
        sem_wait(locks->data);
        fprintf(stderr,"\tReader %d read %d\n", id, i);
        sem_post(locks->data);
    }
    exit(0);
}

void write_mem(memstat *locks, int id){
    int i;
    for (i = 0; i < 10; i++) {
        sem_wait(locks->data);
        printf("Writer %d wrote %d\n", id, i);
        sem_post(locks->data);
        sem_post(locks->reader);
        

    }
}
