/*
* Author: Inchika Alan Koroma
* Description: Semaphore
* Date: 07/05/2015
*/

#include "Sema.h"

 void init(semaphore *sema, int initial_value){
// Setup Semaphore
 	pthread_mutex_init(&sema->lock, NULL);
 	sema->value = initial_value;
 	pthread_cond_init(&sema->cond, NULL);

 }

 void destroy(semaphore *sema){
 	// remove semaphore
 	pthread_mutex_destroy(&sema->lock);
 	pthread_cond_destroy(&sema->cond);
 }

 void procure(semaphore *sema){
  	// Wait for lock
 	pthread_mutex_lock(&sema->lock);

 	while(sema->value <= 0){
 		pthread_cond_wait(&sema->cond, &sema->lock);
 	}

 	sema->value--;

 	pthread_mutex_unlock(&sema->lock);
 }

 void vacate(semaphore *sema){
  	// Release semaphore signals

  	pthread_mutex_lock(&sema->lock);

  	sema->value++;

  	pthread_cond_signal(&sema->cond);

  	pthread_mutex_unlock(&sema->lock);

 }

 

