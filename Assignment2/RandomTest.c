/*
* Author: Inchika Alan Koroma
* Description: Reader's & Writer's test program
* Date: 14/05/2015
* 
*/

#include "RandomTest.h"

buffer myBuffer;

int main(int argc, char **argv){
    
     int stopFlag = 0;

    // Set up queues
    dispatch_queue_t producerQueue;
    dispatch_queue_t consumerQueue;

    // Get the global dispatch queue
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

    consumerQueue = dispatch_queue_create("consumer", NULL);
    producerQueue = dispatch_queue_create("producer", NULL);
    
    dispatch_group_t group = dispatch_group_create();


    dispatch_group_async(group, queue, ^{
       
        // Produce here
        dispatch_async(producerQueue, ^{
           

            printf("Launched producer\n");
            
            init_buffer(&myBuffer, 3, 0); 
            
            
            FILE *source = NULL;
           
            source = fopen("/dev/random", "r");
            
            uint32_t dest;

             // while !stop keep producing values
            while(!stopFlag){
                get_random_uint32(source, &dest);
                put_buffer(&myBuffer, &dest);
            }
            
            printf("Stopping producer\n");
            
            // Clean up buffer.
            destroy_buffer(&myBuffer);
        });
        
        dispatch_async(consumerQueue, ^{
            printf("Launched consumer\n");
            while(!stopFlag){
                printf("Consumed: %i\n", (int) get_buffer(&myBuffer));
            }
            printf("Stopping consumer\n");
        });
        
    });
    
    
    // when enter is pressed stop. 
    while (getc(stdin) != '\n');
    printf("STOPED\n");
    stopFlag = 1;

    // Wait for all tasks in the group to be completed.
    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);

    // Clean up the group
    dispatch_release(group);

    return EXIT_SUCCESS;
    
}

void get_random_uint32(FILE *source, uint32_t *dest){
    
    // Reads high quality 16 bit random numbers 
    static const size_t size = sizeof(uint32_t);
    
    fread(dest, size, 1, source);
}

void init_buffer(buffer *myBuffer, int max_size, int min_fill_level){
    // Initialises the buffer.
    myBuffer->buffer = malloc(sizeof(uint32_t) * max_size);
    
    myBuffer->min_fill_level = min_fill_level;
    myBuffer->size = 0;
    myBuffer->max_size = max_size;
    myBuffer->lastItem = myBuffer->buffer;
    myBuffer->recentItem = myBuffer->buffer;
    init_ec(&myBuffer->ec_put);
    init_sequencer(&myBuffer->seq_put);
    init_ec(&myBuffer->ec_take);
    init_sequencer(&myBuffer->seq_take);
}


void destroy_buffer(buffer *myBuffer){
   
    destroy_sequencer(&myBuffer->seq_put);
    destroy_sequencer(&myBuffer->seq_take);
    destroy_ec(&myBuffer->ec_take);
    destroy_ec(&myBuffer->ec_put);

   // Frees buffer allocated memory.
   free(&myBuffer->buffer);
}

void put_buffer(buffer *myBuffer, uint32_t *value){
    // Add something to the buffer.

    printf("Put: $%lx \n", myBuffer->size);

    if (myBuffer->size >= myBuffer->max_size){ 
        // wiat till free space is avaliable.
         printf("Wait to fill\n");
        await(&myBuffer->ec_take, ticket(&myBuffer->seq_take)); 
    }

    *(myBuffer->recentItem++) = *value;
    myBuffer->size++;
    // inform tread of availiable data
    advance(&myBuffer->ec_put);
}

uint32_t get_buffer(buffer *myBuffer){
    
    // returns the oldest value in buffer

    printf("Get: $%lx \n", myBuffer->size);

    if ((myBuffer->size - 1) <= myBuffer->min_fill_level){ 
    // Block if the minimum_fill_level is going to be broken.
        printf("Wait to consume\n");
        await(&myBuffer->ec_put, ticket(&myBuffer->seq_put));
    }

    myBuffer->size--;

    int r = *myBuffer->lastItem;
    myBuffer->lastItem++;

    // inform tread of availiable data
    advance(&myBuffer->ec_take); 
    return r;
}


void init_sequencer(sequencer *seq){
    
    seq->value = 0;
    init(&seq->sema, 1); 
}

void destroy_sequencer(sequencer *seq){
    destroy(&seq->sema);
}

int ticket(sequencer *seq){
   // returns current value of sequencer seq, and increments++. 
   int16_t value;
   procure(&seq->sema);
   value = seq->value++;
   vacate(&seq->sema);
   return value;
}


void init_ec(event_counter *ec){
    // Initialises an event counter.
    ec = malloc(sizeof(*ec));
    init(ec, 0);
}

void destroy_ec(event_counter *ec){
   // Destroys event counter, de-allocates memory.
   destroy((semaphore *) ec);
}

int myRead(event_counter *ec){
   // Returns the current integer value of the event_counter ec.
   // Atomicity concerns here?
   return ec->value;
}


void advance(event_counter *ec){
    // Atomically increments event counter by 1.

    vacate((semaphore*) ec);
}

void await(event_counter *ec, int value){
    // Blocks current thread until Eventcounter >= value.

    pthread_mutex_lock(&ec->lock);
    if (value >= 0){
        while(ec->value <= value){
            pthread_cond_wait(&ec->cond, &ec->lock);
        }
    } else {
        while(ec->value >= value){
            pthread_cond_wait(&ec->cond, &ec->lock);
        }
    }
    
    pthread_mutex_unlock(&ec->lock);
}
