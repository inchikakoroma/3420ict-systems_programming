#include <pthread.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "Sema.h"


void *threadFunction();


typedef struct {
    char *inPut;
    semaphore printer; 
    semaphore reader;
    int valid;
    // exit flag
    int exit;
} targs; 


int main(int argc, char **argv){
    
    pthread_t childthread; 

    size_t len = 0;

    targs child_args;

    // set up semaphores
    init(&child_args.printer, 0);
    init(&child_args.reader, 0);

    child_args.exit = 0;

    // create a thread
    // call the threadFunction
    int myThread = pthread_create(&childthread, NULL, threadFunction, (void *) &child_args);

    // error check
    if (myThread != 0){
        fprintf(stderr, "create thread fail %d\n", myThread);
        return EXIT_FAILURE;
    }

    printf("Enter some text\n");
    if (getline(&child_args.inPut, &len, stdin) == -1){
         //  getline error check
         perror("getline()");
    }

    child_args.valid = 1;

    // signal child to print
    vacate(&child_args.printer);
    // wait for child printed signal
    procure(&child_args.reader); 

    
    printf("Press Enter to exit");
    while (getc(stdin) != '\n');

    // remove lock from child
    child_args.exit = 1;

    // signal child to unblock, vacate & exit
    vacate(&child_args.printer);

    // wait for child thread 
    while(pthread_join(childthread, NULL) != 0); 

    // clean semaphores
    destroy(&child_args.reader);
    destroy(&child_args.printer);

    if (child_args.inPut != NULL){
        free(child_args.inPut);
    }

    return EXIT_SUCCESS;
}

void *threadFunction(targs *thread1){
    
    
    printf("Child thread will sleep (5 seconds)\n");

    sleep(5);
 
    while(1){

    	// wait for signal to print
        procure(&thread1->printer);

        
        if(thread1->exit == 1){ 

        	 // unlock 
            vacate(&thread1->printer);
            break;
        }

        if (thread1->inPut != NULL){
        	// print thread out
            fputs(thread1->inPut, stdout);
        }

        // signal consumer
        vacate(&thread1->reader); 
        
    }

    pthread_exit(NULL);
}
