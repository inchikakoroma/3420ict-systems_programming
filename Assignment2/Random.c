/*
* Author: Inchika Alan Koroma
* Description: Reader's & Writer's 
* Date: 14/05/2015
* 
*/

#include "Random.h"


int main(int argc, char **argv){
    
	// Reads high quality 16 bit random numbers 
    FILE *source = fopen("/dev/random", "r");
    
    uint32_t dest;
    
    while(1){
        get_random_uint32(source, &dest); 
        printf("%x\n", dest);
    }
    

    return EXIT_SUCCESS; 
}

void get_random_uint32(FILE *source, uint32_t *dest){
    
    // Reads high quality 16 bit random numbers 
    static const size_t size = sizeof(uint32_t);
    //returns number of successful blocks
    fread(dest, size, 1, source); 
}


void init_buffer(buffer *buff, int max_size, int min_fill_level){
    // Initialises the buffer.

    buff->buffer = malloc((size_t)(sizeof(uint32_t) * max_size));
    buff->min_fill_level = min_fill_level;
    buff->size = 0;
    buff->max_size = max_size;
    buff->lastItem = buff->buffer;
    buff->recentItem = buff->buffer;
    init_ec(buff->ec);
    init_sequencer(buff->seq);
}


void destroy_buffer(buffer *buff){
   // Frees buffer allocated memory.
   free(&buff->buffer);
}

void put_buffer(buffer *buff, uint32_t *value){
    // Add something to the buffer.

    if (buff->size >= buff->max_size){ 
    	// wiat till free space is avaliable.
        await(buff->ec, ticket(buff->seq)); 
    }

    *(buff->recentItem++) = *value;
    buff->size++;
    // inform tread of availiable data
    advance(buff->ec);
}

uint32_t get_buffer(buffer *buff){
    
	// returns the oldest value in buffer

    if ((buff->size - 1) <= buff->min_fill_level){ 
    // Block if the minimum_fill_level is going to be broken.
        await(buff->ec, ticket(buff->seq));
    }

    buff->size--;
    // inform tread of availiable data
    advance(buff->ec); 
    return *(buff->lastItem++);
}

// Sequencers are closely linked with event counters. A Sequencer is also an integer (initialised to 0) that is used to serialise the occurrence of events. A sequencer only has one atomic operation, ticket(), that returns the current value of the sequencer and then increments the sequencer by one.

void init_sequencer(sequencer *seq){
    seq->value = 0;
    init(&seq->sema, 1); 
}

void destroy_sequencer(sequencer *seq){
    destroy(&seq->sema);
}

int ticket(sequencer *seq){
   // returns current value of sequencer seq, and increments++. 
   int32_t value;
   procure(&seq->sema);
   value = seq->value++;
   vacate(&seq->sema);
   return value;
}


void init_ec(event_counter *ec){
    // Initialises an event counter.
    init((semaphore*) ec, 0);
}

void destroy_ec(event_counter *ec){
   // Destroys event counter, de-allocates memory.
   destroy((semaphore *) ec);
}

void advance(event_counter *ec){
    // Atomically increments event counter by 1.

    vacate((semaphore*) ec);
}

void await(event_counter *ec, int value){
    // Blocks current thread until Eventcounter >= value.

    pthread_mutex_lock(&ec->lock);
    if (value >= 0){
        while(ec->value <= value){
            pthread_cond_wait(&ec->cond, &ec->lock);
        }
    } else {
        while(ec->value >= value){
            pthread_cond_wait(&ec->cond, &ec->lock);
        }
    }
    
    pthread_mutex_unlock(&ec->lock);
}