/*
* Author: Inchika Alan Koroma
* Description: A Simple Command Line Interface
* Date: 30/04/2015
* Does not work properly 
*/

#include "Sync.h"

int main(int argc, char **argv){

    // call the filestream function
    //fileStream(stdin, stdout);

    pthread_t childThread;

    //char *inPut = NULL;
    //size_t inPut_len = 0;

    targs childArgs;
    
    // Set up locking mutex
    pthread_mutex_init(&childArgs.lock, NULL); 
    // Set up printing lock
    pthread_mutex_init(&childArgs.output_lock, NULL); 

    // lock
    pthread_mutex_lock(&childArgs.lock);
    // Lock until priting is done
    pthread_mutex_lock(&childArgs.output_lock); 

    childArgs.exit = 0;

    // create a thread
    int myThread = pthread_create(&childThread, NULL, threadFunction, (void *) &childArgs);
    
    // error check
    if (myThread != 0){ 
        fprintf(stderr, "create thread fail %d\n", myThread);
        return EXIT_FAILURE;
    }


    fileStream(stdin, stdout);
    //fgets(&childArgs.inPut, &inPut_len, stdin);
    //getline(&childArgs.inPut, &inPut_len, stdin);

    // Let the child thread print
    pthread_mutex_unlock(&childArgs.lock); 

    // Wait for child to print
    while(pthread_mutex_trylock(&childArgs.output_lock) != 0);
   
    
    puts("Press Enter");
    while (getc(stdin) != '\n');

    // remove lock from child
    childArgs.exit = 1;

    // wait for child thread
    while(pthread_join(childThread, NULL) != 0); 

    pthread_mutex_destroy(&childArgs.lock);
    pthread_mutex_destroy(&childArgs.output_lock);
   
    //puts("Parent is back");

    if(childArgs.inPut != NULL){
        free(childArgs.inPut);
    }

    return EXIT_SUCCESS;
}



void *threadFunction(targs *thread1) {
  
  while(thread1->exit == 0){
        if(pthread_mutex_trylock(&thread1->lock) == 0){
            if (thread1->inPut != NULL){
                fputs(thread1->inPut, stdout);
            }
            pthread_mutex_unlock(&thread1->output_lock);
        }
    }
    
  
  return NULL;
}


int fileStream(FILE *inPut, FILE *outPut){

    while(fgets(streamBuff, sizeof streamBuff, inPut) != NULL){

        fputs(streamBuff, outPut); 

        // check for error
        if (ferror(inPut) != 0){
            
            perror("File Stream Error");
            
            return -1;
        }
    }

    fclose(inPut);

    return EXIT_SUCCESS;
}



