#define SEMA_H
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/sem.h>

// semaphore struct
typedef struct semaphore {
	pthread_mutex_t lock;
	int value;
	pthread_cond_t cond;
} semaphore;

 // Semaphore Methods

 void procure(semaphore *s);
 void vacate(semaphore *s);

 void init(semaphore *sema, int initial_value);
 void destroy(semaphore *s);
 

