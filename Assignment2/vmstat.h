//  Created by Rene Hexel on 11/05/2015.
//  Copyright (c) 2015 Rene Hexel. All rights reserved.
//	Edited by Inchika Alan Koroma on 22 May 2015

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/time.h>

#ifndef __VMStat__vmstat__
#define __VMStat__vmstat__
#define _POSIX_C_SOURCE 200112L
#define DEFAULT_PAGE_SIZE   4096    // default page size if unknown
#define BUFFER_SIZE         80      // command and response buffer size

// vmstat or vm_stat commands to run
#define VMSTAT  "vmstat -SK  2>/dev/null | tail -n1 | sed 's/  */\t/g' | cut -f5"
#define VM_STAT "vm_stat -c 1 1  2>/dev/null | tail -n1 | sed 's/  */\t/g' | cut -f2"
#define VM_PAGE "vm_stat  2>/dev/null | head -n1 | cut -d' ' -f8"



///
/// A simple data structure for POSIX virtual memory statistics
///
typedef struct memstat
{
    long available_pages;   ///< number of pages freely available
    long page_size;         ///< virtual memory page size in bytes
    sem_t *data;
    sem_t *reader;
} memstat;



///
/// return the memory currently available in the system
///
memstat available_memory(void);

void read_mem(memstat *locks, int id);
void write_mem(memstat *locks, int id);


#endif /* defined(__VMStat__vmstat__) */
