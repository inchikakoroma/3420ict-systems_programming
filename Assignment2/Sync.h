#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


// file stream 
int fileStream(FILE *inPut, FILE *outPut);

// thread function
void *threadFunction();



// buffer with 128 characters
char streamBuff[128];

// thread struct 

typedef struct {
	char *inPut;
	pthread_mutex_t lock;
    pthread_mutex_t output_lock;
    int exit;
} targs;

