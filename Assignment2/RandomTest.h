#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <dispatch/dispatch.h>


#include "Sema.h"

void get_random_uint32(FILE *source, uint32_t *dest);



// An Event Counter
typedef semaphore event_counter;

typedef struct Sequencer {  
    semaphore sema; 
    int value;
} sequencer;


typedef struct buffer {
    uint32_t *buffer;
    int max_size;
    int min_fill_level;
    long size;
    // last item (head)
    uint32_t *lastItem; 
    // recent item (tail)
    uint32_t *recentItem;
    event_counter ec_put;
    sequencer seq_put;
    event_counter ec_take;
    sequencer seq_take;
} buffer;

void init_buffer(buffer *buff, int max_size, int min_fill_level); 
void destroy_buffer(buffer *buff); 

// Put items into the buffer
void put_buffer(buffer *buff, uint32_t *value);
// Get the most resent item
uint32_t get_buffer(buffer *buff);


void init_sequencer(sequencer *seq);
void destroy_sequencer(sequencer *seq);

int ticket(sequencer *seq);

void init_ec(event_counter *ec);
void destroy_ec(event_counter *ec);
int myRead(event_counter *ec);
void advance(event_counter *ec);
void await(event_counter *ec, int value);
