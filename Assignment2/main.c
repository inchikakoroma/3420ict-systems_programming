//
//  main.c
//  VMStat
//
//  Created by Rene Hexel on 11/05/2015.
//  Copyright (c) 2015 Rene Hexel. All rights reserved.
//
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "vmstat.h"

#define MB  (1024.0*1024.0)             // number of bytes per megabyte

int main(int argc, const char * argv[])
{

	memstat sem;
	sem_unlink(":Test1");
	sem.data = sem_open(":Test1", O_CREAT, S_IRUSR | S_IWUSR, 0);
	sem_post(sem.data); // initially open
    memstat info = available_memory();  // get available memory info

    if (info.available_pages <= 0)
    {
        perror("Cannot get available memory");
        exit(EXIT_FAILURE);
    }

    // The process PID
    pid_t pid;
    
    // clone fork
    pid = fork();

    // check fork()
    if (pid == -1){
        
    	perror("fork");
        exit(EXIT_FAILURE);
    }
    
    if (!pid) read_mem(&sem, 1);
    else {
        pid = fork();
        if (!pid) write_mem(&sem, 1);
        else {
            pid = fork();
            if (!pid) read_mem(&sem, 2);
            else write_mem(&sem, 2);
           // sleep(10);
            sem_close(sem.data);
            sem_close(sem.reader);
        }
    }

    long long bytes = (long long) info.available_pages * info.page_size;

    printf("%lld bytes (%g MBytes) available (%ld pages of %ld bytes)\n", bytes, bytes / MB, info.available_pages, info.page_size);

    return EXIT_SUCCESS;
}
