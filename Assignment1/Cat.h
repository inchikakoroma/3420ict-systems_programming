#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>
#include <getopt.h>
#include <string.h>
#define SQUEEZE_LEN 1024 


// Argument Methods
void myGetOpt(int argc, char **argv);
int fileInput(FILE *inPut, FILE *outPut);
void showLineNumber(char inPut, FILE *outPut);
void showDollar(char inPut, FILE *outPut);
void showTabChars(char inPut, FILE *outPut);
void numberNonBlankLines(char inPut, FILE *outPut);
void squeezeLines(char inPut, FILE *outPut);
void nonPrintingChars(char inPut, FILE *outPut);
void showNonprintDollar(char inPut, FILE *outPut);


// checks if the user types -n
int myNFlagChecker = 0;

// checks if the user types -E
int myEFlagChecker = 0;

// checks if the users types -v
int myVFlagChecker = 0;

// checks if the user types -e
int myEEFlagChecker = 0;

// checks if the user types -b
int myBFlagChecker = 0;

// checks if the user types -s
int mySFlagChecker = 0;

// checks if the user types -t
int myTFlagChecker = 0;

// check current char
int currentChar = 0;

// check if its a new line
int previousChar = '\n';

// check next character
int nextChar = 0;

// line number count
int lineNumber = 0;

// Max line lenght
char line[SQUEEZE_LEN]; 
