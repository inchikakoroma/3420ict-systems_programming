/*
* Author: Inchika Alan Koroma
* Description: A Simple Command Line Interface
* Date: 28/03/2015
*/

#include "Shell.h"

int main(int argc, char **argv){

    int chars = fileInPut(stdin, stdout);
    putchar(chars);
    return (0);

}

void clean(){
   // int i = 0; // Local iterator, remember to 0 before use.

    // Do cleanup.
    if (commandLine != NULL){
        free(commandLine);
        commandLine = NULL;
    }

    while (args != NULL){
        free(args);
        args++;
    }

    args = NULL;

    return;
}

// custon file method
int fileInPut(FILE *inPut, FILE *outPut){

    if(fprintf(outPut, "bash: ") < 0){
            fprintf(stderr, "output ERROR");
    }

    while((lineChars = getline(&commandLine, &buff, inPut)) != -1){

        if (strcmp(commandLine, "exit\n") != 0){
             mySystem(commandLine);
        } else {
            clean();
            // return if user exits cd
            return EXIT_SUCCESS;
        }

        if(fprintf(outPut, "bash: ") < 0){
            fprintf(stderr, "output ERROR");
        }
        
    }
    clean();
    return EXIT_SUCCESS;
}

// store each word to an Array method
void myCommandLine(char *inPut, char **args){

     if(*(inPut + strlen(inPut) - 1) == '\n'){
        *(inPut + strlen(inPut) - 1) = '\0'; 
    }

    // Check to see if the line is terminated with '&' and change waiting flag in response.
    if (*(inPut + strlen(inPut) - 1) == '&'){
        *(inPut + strlen(inPut) -1) = '\0';
        waitforchild = 0;
    } else {
        waitforchild = 1;
    }

    char myChars;
    char *myLine = inPut;
    int size = 30;
    char *buffer = malloc(size*sizeof(char));

    if(buffer == NULL){
        fprintf(stderr, "malloc error in commandLine()");
        abort();
    }

    char *t;
    // Array position
    int position = 0;
    // String position
    int myString = 0;
    int lastblank = 1;

    while(args[position] != (char*) NULL){
        free(args[position++]);
    }

    position = 0;

    myChars = *myLine;
    // Get first char of input line.

    while (myChars != '\0'){
        if (myChars != ' ' && myChars != '\t'){
            if (lastblank == 1){ 
                lastblank = 0;
            }
            if (myString >= size){
                size += 20;
                t = realloc(buffer, size * sizeof(char*));
                if(t != NULL){
                    buffer = t;
                } else {
                    fprintf(stderr, "Error.\n");
                    abort();
                }
            }
            *(buffer + myString++) = myChars;
        } else {
           lastblank = 1;
        }
          
        myChars = *(++myLine);
        if (lastblank == 0 && (myChars == ' ' || myChars == '\t' || myChars == '\0')){
            *(buffer + myString) = '\0';
            args[position++] = buffer;
            buffer = malloc(size*sizeof(char));
            myString = 0;
        }
    }

    // The execvp argv must be terminated by a null pointer.
    args[position] = (char *) NULL;

    return; 
}

int countsubstrs(char *line){
    // Return the number of substrings in the given string.
    char c;
    char *l = line;
    int i = 0;
    int lastblank = 1;

    while ((c = *(l++)) != '\0'){
        if (lastblank && !(c == ' ' || c == '\t')){
           i++;
           lastblank = 0; 
        } else {
           if(c == ' ' || c == '\t' || c == '\n'){
               lastblank = 1;
           } else {
               lastblank = 0;
           }
        }
    }

    return i;
}

// behaves like system() but uses execvp()
int mySystem(char *cmLine){

    int len = countsubstrs(cmLine);
    args = malloc((len + 1) * sizeof(char*));

    // The child process PID
    pid_t chpid;
    
    // childStatus
    int childStatus;
    
    // call the argument array
    myCommandLine(cmLine, args);

    // clone fork
    chpid = fork();
     
    if (chpid == -1){
        // check fork()
        return -1; 
    }

    if (chpid == 0){ 


        if(strcmp(args[0], "cd") == 0){
            changeDir();
        }else{
            execvp(args[0], args);
            perror("Execvp Error");

            if(kill(getpid(), SIGTERM) != 0){
                perror("CD Directory Error");
            }

            return -1;
       } 
        

    } else { 

        if (!waitforchild){
            signal(SIGCHLD, SIG_IGN);
        }else{
            signal(SIGCHLD, SIG_DFL);
        }

        if(waitforchild){
            while(wait(&childStatus) != chpid);
        }
        
    }

    return 1;
}



int changeDir(){

    if(args[1] != (char *) NULL){
        if(chdir(args[1]) == -1){
            perror("Change Directory Error");
        }
    } else{
        fprintf(stderr, "Command = 'cd");
        return EXIT_FAILURE;
    }


    return EXIT_SUCCESS;
}

