[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

MILESTONES 1 - 3
==================

[Hello World](https://bitbucket.org/inchikakoroma/3420ict-systems_programming/src/9c57c3db5df97a128ca3e12405a601e0d109c797/Hello.c?at=master) This program simply outputs the word HELLO

[Cat](https://bitbucket.org/inchikakoroma/3420ict-systems_programming/src/9c57c3db5df97a128ca3e12405a601e0d109c797/Cat.c?at=master) My Cat program copies the functionaly of the GNU Cat 

MILESTONE 4
==================

[Shell](https://bitbucket.org/inchikakoroma/3420ict-systems_programming/src/9c57c3db5df97a128ca3e12405a601e0d109c797/Shell.c?at=master)

The first task of my program was to use the fgets() function to read a simgle line input from the user. The input is then displayed back for debugging purposes.

The second task of the shell program was to implement  a simple exit command. When the user types "exit" the program exits.

The next task was to use the system() function to execute simple command line commands.

The next step was to use the execvp() function to execute commands.
system() function calls out to shell to handle your command line, so you can get wildcard expansion. 
exec() function and its friends replace the current process image with a new process image.
With exec() function, your process is obliterated.
system() function causes a child process to be created, and the exec() function family do not. You would need to use fork() for that.