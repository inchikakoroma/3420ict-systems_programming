
#define SH_H
#define _GNU_SOURCE


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <signal.h>
#include <ctype.h>


// Argument Methods
int fileInPut(FILE *inPut, FILE *outPut);
int mySystem(char *cmLine);
void myCommandLine(char *inPut, char **args);
int changeDir();
void clean();

FILE *inPut = NULL;
FILE *outPut = NULL;

// input buffer
size_t buff = 0;

// The arguments array
char **args;
size_t lineChars = 0;

char *commandLine = NULL;

int waitforchild = 1;
