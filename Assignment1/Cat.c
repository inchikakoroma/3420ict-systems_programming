/*
* Author: Inchika Alan Koroma
* Description: A simple clone of Cat
* Date: 8/03/2015
*/

#include "Cat.h"

int main(int argc, char **argv){

	myGetOpt(argc, argv);
	int chars = fileInput(stdin, stdout);
	putchar(chars);
	
	return 1;
	
}

// Commandline Argument method
void myGetOpt(int argc, char **argv){

	char opt;

	while((opt = getopt(argc, argv, "nEvebst")) != -1){
			// A switch to check the command the user enters
			switch (opt){
			case 'n':
			myNFlagChecker = 1;
			break;
			case 'E':
			myEFlagChecker = 1;
			break;
			case 'v':
			myVFlagChecker = 1;
			break;
			case 'e':
			myEEFlagChecker = 1;
			break;
			case 'b':
			myBFlagChecker = 1;
			break;
			case 's':
			mySFlagChecker = 1;
			break;
			case 't':
			myTFlagChecker = 1;
			break;
			default:
			fprintf(stderr, "Display -n for Line Number\n -E for $\n -b for Non-Blank line\n -s for Squeeze Empty Lines\n -v for Display Non Printing Characters\n -t for Displaying Tab Characters\n -e for both -E & -v functions\n");
			printf("Continue without Argument or Press CTRL D to quit\n");
			break;
			}
	}
	

}

int fileInput(FILE *inPut, FILE *outPut){

	while (feof(inPut) == 0){
		currentChar = fgetc(inPut);
		
		if(myNFlagChecker == 1 && myBFlagChecker == 1){
			// if the user enters both -n & -b as arguments
			// turn off line numbers for -n
			// the lines still gets numbered from -b
			myNFlagChecker = 0;
		}
		if (myNFlagChecker == 1 && previousChar == '\n'){
			// call the showLineNumber method
			showLineNumber(currentChar, outPut);
		}
		if (myEFlagChecker == 1){
			// call showDollar method
			showDollar(currentChar, outPut);
		}
		if (myTFlagChecker == 1 && previousChar == '\n'){
			// call the show tab method
			showTabChars(currentChar, outPut);
		}
		if (myBFlagChecker == 1  && previousChar == '\n'){
			// call the number non blank line
			numberNonBlankLines(currentChar, outPut);
		}
		if (mySFlagChecker == 1 && previousChar == '\n'){
			// call the squeeze line method
			squeezeLines(currentChar, outPut);
		}
		if (myVFlagChecker == 1 && previousChar == '\n'){
			// call the non print chars method
			nonPrintingChars(currentChar, outPut);
		}
		if (myEEFlagChecker == 1 && previousChar == '\n'){
			// call the showNonPrinting method
			showNonprintDollar(currentChar, outPut);
		}
		
		fputc(currentChar, outPut);

		previousChar = currentChar;
		//fputc(getc(inPut), outPut);
	}


	fclose (inPut);

	return 1;
}

// show line number
void showLineNumber(char inPut, FILE *outPut){
	fprintf(outPut, "%6d\t", ++lineNumber);
}

// show $ sign method
void showDollar(char inPut, FILE *outPut){
	fputc('$', outPut);
}

// show tab character method
void showTabChars(char inPut, FILE *outPut){
		// non printable 
	   	if(inPut <= 32 && previousChar == '\n'){
		// print out the first 64 characters
      	fprintf(outPut, "^%c", inPut + 64);
   		}else if(inPut < 127 && previousChar == '\n'){
   		// print out the rest of the ASCII characters
      	putc(inPut, outPut);
   		}
   		// tab char
   		if(currentChar == '\t'){
			char *tabChar = "";
			// display tab character as ^|
			fprintf(outPut, "^|%s^|", tabChar);
			
	   	}

	// display error message
	if (ferror(outPut) != 0){
		printf("Display Tab Error");
	}
}

// number non blank lines
void numberNonBlankLines(char inPut, FILE *outPut){
	if((previousChar == '\n' || previousChar == 0) && currentChar != '\n'){
		fprintf(outPut, "%i\t", ++lineNumber);
	}
}

// squeeze multiple adjacent empty lines
void squeezeLines(char inPut, FILE *outPut){
	
	
	char *myChars;
	 while (fgets(line, SQUEEZE_LEN, stdin) != NULL) { 
        // remove newline, and trailing whitespace, if any
        *(strchr(line, '\n')) = '\0'; 
        for (myChars = strchr(line, '\0') - 1; (myChars >= line) && (isspace(*myChars)); --myChars) { 
            *myChars = '\0'; 
        } 

        /* if not blank, display it */ 
        if (strlen(line) > 0) { 
            printf("%s\n", line); 
        } 
    } 
    
    return;


}

// Display non Printing ASCII characters 
void nonPrintingChars(char inPut, FILE *outPut){

	if(inPut <= 32 ){
		// print out the first 64 characters
      	fprintf(outPut, "^%c", inPut + 64);
   	}else if(inPut < 127){
   		// print out the rest of the ASCII characters
      	putc(inPut, outPut);
   	}else if(inPut == 0x7f){
   		// just print DEL for the delete character
        puts("^DEL");
  	}
	return;
}

// Combines the -E argument (display $) with -v argument (display non printing characters)
void showNonprintDollar(char inPut, FILE *outPut){
	
	if(inPut <= 32 ){
		// print out the first 64 characters and put a $ at the end
      	fprintf(outPut, "^%c$", inPut + 64);
   	}else if(inPut < 127){
   		// print out the rest of the ASCII characters
      	fprintf(outPut, "^%c$", inPut);
   	}
	return;
}

